<?php
/*
  Plugin Name: Mass Relevance Space Widget
  Plugin URI: http://www.bitbucket.org/_trey/mass-relevance-space-widget
  Description: This widget allows you to incorporate a Mass Relevance Space into a page.
  Author: The Marketing Arm
  Version: 1.0
  Author URI: http://www.themarketingarm.com
*/

class Mass_Relevance_Space_Widget extends WP_Widget {

  public function __construct() {
    $widget_opts = array(
      'classname'   =>  'mass-relevance-space-widget',
      'description' =>  'A Space from your Mass Relevance account.'
    );

    $control_opts = array(
      'width'   =>  'auto',
      'height'  =>  'auto',
      'id_base' =>  'mass-relevance-space-widget'
    );
    
    parent::__construct(
      'mass-relevance-space-widget',
      'Mass Relevance Space',
      $widget_opts,
      $control_opts
    );
  }

  // Front End Widget Display
  public function widget($args, $instance) {
    $title = apply_filters('widget_title', $instance['title']);
    $space = esc_attr($instance['space']);
    
    echo $args['before_widget'];
    if (!empty($title))
      echo $args['before_title'] . $title . $args['after_title'];
    
    // Widget UI
    ?>
    <div class="mr-space" data-space-id="<?=$space?>"></div>
    <script src="//platform.massrelevance.com/js/massrel.js"></script>
    <script>
      massrel.ui.load();
    </script>
    <?php
    echo $args['after_widget'];
  }
  
  // Back End Widget Form
  public function form($instance) {
    // Title
    if (isset($instance['title']))
      $title = $instance['title'];
    else
      $title = __('Mass Relevance Space', 'mass-relevance-space-widget');
    
    $title_field_id = $this->get_field_id('title');
    $title_field_name = $this->get_field_name('title');
    
    // Space Name
    if (isset($instance['space']))
      $space = $instance['space'];
    
    $space_field_id = $this->get_field_id('space');
    $space_field_name = $this->get_field_name('space');
    
    // Form UI
    ?>    
    <div>
      <label for="<?=esc_attr($title_field_id)?>">Title:</label>
      <input id="<?=esc_attr($title_field_id)?>" name="<?=esc_attr($title_field_name)?>" class="widefat" type="text" value="<?=esc_attr($title)?>" />
    </div>
    <div>
      <label for="<?=esc_attr($space_field_id)?>">Space ID:</label>
      <input id="<?=esc_attr($space_field_id)?>" name="<?=esc_attr($space_field_name)?>" class="widefat" type="text" value="<?=esc_attr($space)?>" />
    </div>
    <?php
  }
  
  // Save Widget Instance Config
  public function update($new_instance, $old_instance) {
    $instance = array();
    $instance['title'] = strip_tags($new_instance['title']);
    $instance['space'] = strip_tags($new_instance['space']);
    return $instance;
  }
}

// Register the Widget with WordPress
function register_mass_relevance_space_widget() {
  register_widget('Mass_Relevance_Space_Widget');
}
add_action('widgets_init', 'register_mass_relevance_space_widget');

?>